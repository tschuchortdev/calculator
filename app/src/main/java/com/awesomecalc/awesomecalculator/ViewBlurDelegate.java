package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;

/**
 * delegate that can be used in a View to do blurred drawing operations or
 * in a ViewGroup to blur the drawing of it'S children
 * <p>
 * Created by Thilo on 2/22/2016.
 */
public class ViewBlurDelegate {
    private Bitmap mBufferBmp;
    private Canvas mBufferCanvas;
    private boolean mBlurred;
    private float mBrighteningFactor = 0.9f;
    private BlurredView mView;

    interface BlurredView {
        /**
         * the View should implement this method and do it's blurred
         * drawing operations in here
         */
        void onDrawBlurred(Canvas canvas);

        Context getContext();
        void invalidate();
    }

    public ViewBlurDelegate(BlurredView delegatorView)
    {
        mView = delegatorView;
    }

    public void setBlurred(boolean blurred)
    {
        mBlurred = blurred; mView.invalidate();
    }

    public boolean isBlurred()
    {
        return mBlurred;
    }

    public void setBrighteningFactor(float brighteningFactor)
    {
        if (brighteningFactor < 0 || brighteningFactor > 1) throw new IllegalArgumentException("brightening factor must be between 0 and 1");

        mBrighteningFactor = brighteningFactor;
    }

    public float getBrighteningFactor()
    {
        return mBrighteningFactor;
    }

    /**
     * call in View.onDraw() or ViewGroup.dispatchDraw()
     * do all your other drawing operations in drawBlurred()
     * operations that shouldn't be blurred out have to be drawn AFTER this method is called
     */
    public void drawBlurred(@NonNull Canvas canvas)
    {
        if(mBlurred) {
            if(mBufferBmp == null || mBufferBmp.getWidth() != canvas.getWidth() || mBufferBmp.getHeight() != canvas.getHeight()) {
                mBufferBmp = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
                mBufferCanvas = new Canvas(mBufferBmp);
            }

            mBufferBmp.eraseColor(0);

            mView.onDrawBlurred(mBufferCanvas);

            Paint p = new Paint();
            p.setColorFilter(UIUtils.createTintFilter(Color.argb((int) (0xFF * mBrighteningFactor), 0xFF, 0xFF, 0xFF)));

            canvas.drawBitmap(UIUtils.blurBitmapDownscaled(mBufferBmp, mView.getContext(), 3, 0.5f), 0.0f, 0.0f, p);
        }
        else
            mView.onDrawBlurred(canvas);
    }

}
