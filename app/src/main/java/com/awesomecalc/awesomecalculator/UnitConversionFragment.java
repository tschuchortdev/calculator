package com.awesomecalc.awesomecalculator;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Thilo on 11/17/2015.
 */
public class UnitConversionFragment extends ActionBarFragment {
    protected View mRoot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRoot = inflater.inflate(R.layout.frag_unit_conversion, container, false);
        return mRoot;
    }

    @Override
    public Toolbar getSupportActionBar()
    {
        if(mRoot == null)
            throw new IllegalStateException("action bar not yet created");

        return (Toolbar) mRoot.findViewById(R.id.toolbar);
    }

    @Override
    public String getTitle()
    {
        return getActivity().getString(R.string.drawer_unit_conversion);
    }
}
