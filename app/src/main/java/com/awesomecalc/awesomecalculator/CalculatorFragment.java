package com.awesomecalc.awesomecalculator;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CalculatorFragment extends ActionBarFragment {
    protected View mRoot;
    @Bind(R.id.ripple_fill_view) protected RippleFillView mRippleFillView;
    @Bind(R.id.keyboard) protected MathKeyboard mKeyboard;
    @Bind(R.id.main_math_view) protected MathView mMathView;
    @Bind(R.id.scroll_view) protected HorizontalScrollView mScrollView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRoot = inflater.inflate(R.layout.frag_calculator, container, false);
        ButterKnife.bind(this, mRoot);

        mKeyboard.setListener(new MathKeyboard.MathKeyboardListener() {
               @Override
               public void onKeyPressed(String input)
               {}

               @Override
               public void onSolvePressed()
               {
                   int colorError = UIUtils.getColorFromTheme(getActivity(), R.attr.colorError);
                   int colorErrorDark = UIUtils.getColorFromTheme(getActivity(), R.attr.colorErrorDark);

                   mRippleFillView.setColor(colorError);
                   mRippleFillView.setOrigin(mKeyboard.getMeasuredWidth() * 3 / 4, mRippleFillView.getMeasuredHeight());
                   mRippleFillView.rippleWithEndAction(() ->
                        {
                            setActionBarColor(colorError);
                            setStatusBarColor(colorErrorDark);
                        });
               }

               @Override
               public void onBackspacePressed()
               {}

               @Override
               public void onClearPressed()
               {
                   int colorPrimary = UIUtils.getColorFromTheme(getActivity(), R.attr.colorPrimary);

                   mRippleFillView.setColor(colorPrimary);
                   mRippleFillView.setOrigin(mKeyboard.getMeasuredWidth() * 5 / 8, mRippleFillView.getMeasuredHeight());
                   mRippleFillView.rippleWithEndAction(() ->
                   {
                       setActionBarColor(colorPrimary);
                       setStatusBarColor(Color.TRANSPARENT);
                       mMathView.setText("");
                   });
               }

               @Override
               public void onMoveCursorRightPressed()
               {}

               @Override
               public void onMoveCursorLeftPressed()
               {}
        });

        return mRoot;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.actionbar_main, menu);
    }

    @Override
    public Toolbar getSupportActionBar()
    {
        if(mRoot == null)
            throw new IllegalStateException("action bar not yet created");

        return (Toolbar) mRoot.findViewById(R.id.toolbar);
    }

    @Override
    public String getTitle()
    {
        return getActivity().getString(R.string.drawer_calculator);
    }
}
