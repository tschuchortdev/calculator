package com.awesomecalc.awesomecalculator;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.AnimatorRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

/**
 * Created by Thilo on 4/19/2016.
 *
 * implements drawer behaviour for an activity with fragments
 */
public class FragmentChangerDelegate {
    Activity mActivity;
    int mFragmentContainerId;
    int mFragmentAnimationIn = android.R.animator.fade_in;
    int mFragmentAnimationOut = android.R.animator.fade_out;
    OnFragmentChangedListener mFragmentChangedListener;


    public interface OnFragmentChangedListener {
        void onFragmentChanged(Fragment newFragment);
    }

    public FragmentChangerDelegate(@NonNull Activity activity, @IdRes int fragmentContainer)
    {
        mActivity = activity;
        mFragmentContainerId = fragmentContainer;
    }

    /**
     * call in Activity.onBackPressed()
     * @return true if the back press was handled. You may call super.onBackPressed
     * if it returns false
     */
    public boolean onBackPressed()
    {
        //if fragment back stack is not empty -> open previous fragment
        if(mActivity.getFragmentManager().getBackStackEntryCount() > 0) {
            mActivity.getFragmentManager().popBackStack();
            return true;
        }

        return false;
    }

    public void displayFragment(final Fragment newFrag)
    {
        //collision unlikely due to low object count
        String tag = Integer.toBinaryString(System.identityHashCode(newFrag));

        //don't do anything when the new fragment is already being displayed
        if(getCurrentFragment() != null && getCurrentFragment().getTag().equals(tag))
            return;

        FragmentTransaction fragTransaction = mActivity.getFragmentManager().beginTransaction();

        //set animations before replacing!
        fragTransaction.setCustomAnimations(mFragmentAnimationIn, mFragmentAnimationOut);

        fragTransaction.replace(mFragmentContainerId, newFrag, tag);

        //don't add the first fragment to the backstack
        if(getCurrentFragment() != null)
            fragTransaction.addToBackStack(null);

        fragTransaction.commit();
        mActivity.getFragmentManager().executePendingTransactions();

        if(mFragmentChangedListener != null)
            mFragmentChangedListener.onFragmentChanged(newFrag);
    }

    public void setOnFragmentChangedListener(OnFragmentChangedListener listener)
    {
        mFragmentChangedListener = listener;
    }

    public void setFragmentAnimations(@AnimatorRes int in, @AnimatorRes int out)
    {
        mFragmentAnimationIn = in;
        mFragmentAnimationOut = out;
    }

    public Fragment getCurrentFragment()
    {
        return mActivity.getFragmentManager().findFragmentById(mFragmentContainerId);
    }
}
