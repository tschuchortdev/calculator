package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by Thilo on 16.02.2015.
 *
 * contents/childs of this layout are blurred when called setBlurred(true)
 * other classes can also delegate the blurring to ViewBlurDelegate and bypass the need for an extra layout
 */
public class BlurLayout extends FrameLayout implements ViewBlurDelegate.BlurredView {
    private ViewBlurDelegate mImpl;

    public BlurLayout(Context ctx)
    {
        this(ctx, null);
    }

    public BlurLayout(Context ctx, AttributeSet attrs)
    {
        this(ctx, attrs, 0);
    }

    public BlurLayout(Context ctx, AttributeSet attrs, int defStyle)
    {
        super(ctx, attrs, defStyle);
        mImpl = new ViewBlurDelegate(this);

        if(attrs != null) {
            TypedArray typedAttrs = getContext().obtainStyledAttributes(attrs, R.styleable.BlurLayout, 0, 0);

            if(typedAttrs != null) {
                mImpl.setBlurred(typedAttrs.getBoolean(R.styleable.BlurLayout_blurred, false));
                mImpl.setBrighteningFactor(typedAttrs.getFloat(R.styleable.BlurLayout_brighteningFactor, 0.3f));
                typedAttrs.recycle();
            }
        }
    }

    public void setBlurred(boolean blurred)
    {
        mImpl.setBlurred(blurred);
    }

    public boolean isBlurred()
    {
        return mImpl.isBlurred();
    }

    public void setBrighteningFactor(float factor)
    {
        mImpl.setBrighteningFactor(factor);
    }

    public float getBrighteningFactor()
    {
        return mImpl.getBrighteningFactor();
    }

    @Override
    public void onDrawBlurred(Canvas canvas)
    {
        //draw children blurred
        super.dispatchDraw(canvas);
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        if(BuildFlags.BLUR)
           mImpl.drawBlurred(canvas);
        else
            super.dispatchDraw(canvas);
    }
}
