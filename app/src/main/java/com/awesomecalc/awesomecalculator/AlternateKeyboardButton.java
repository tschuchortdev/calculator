package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

/**
 * Created by Thilo on 1/4/2016.
 */
public class AlternateKeyboardButton extends Button implements KeyboardButton.AlternateButton, View.OnClickListener {
    protected String mKeyCode;
    protected KeyboardButton mParentButton;

    public AlternateKeyboardButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.AlternateKeyboardButton, 0, 0));
    }

    public AlternateKeyboardButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.AlternateKeyboardButton, 0, 0));
    }

    private void init(TypedArray typedAttrs)
    {
        if(typedAttrs != null) {
            mKeyCode = typedAttrs.getString(R.styleable.AlternateKeyboardButton_keyCode);

            if(mKeyCode == null) {
                throw new IllegalStateException("AlternateKeyboardButton must be created with keyCode attribute");
            }
        }
        else {
            throw new NullPointerException("AlternateKeyboardButton must have attributes");
        }

        setOnClickListener(this);
    }

    @Override
    public void setParentButton(KeyboardButton parentButton)
    {
        mParentButton = parentButton;
    }

    public void setKeyCode(@NonNull String newKeyCode)
    {
        mKeyCode = newKeyCode;
    }

    public String getKeyCode()
    {
        return mKeyCode;
    }

    @Override
    public void onClick(View v)
    {
        mParentButton.getParentMathKeyboard().keyCodeClicked(mKeyCode);
    }
}
