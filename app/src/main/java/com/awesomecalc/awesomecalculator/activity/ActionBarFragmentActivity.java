package com.awesomecalc.awesomecalculator.activity;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

/**
 * Created by Thilo on 4/19/2016.
 *
 * Activity that has no actionbar of it's own and uses ActionBarFragment
 */
public abstract class ActionBarFragmentActivity extends AppCompatActivity {

    @Override
    public void setSupportActionBar(Toolbar toolbar)
    {
        super.setSupportActionBar(toolbar);
        onActionBarChanged(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getCurrentFragment().onCreateOptionsMenu(menu, getMenuInflater());
        return true;
    }

    protected abstract Fragment getCurrentFragment();


    @Override
    public void setActionBar(android.widget.Toolbar toolbar)
    {
        throw new IllegalArgumentException("should use support toolbar");
    }

    protected void onActionBarChanged(Toolbar actionBar)
    {}
}
