package com.awesomecalc.awesomecalculator.activity;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.awesomecalc.awesomecalculator.ActionBarFragment;
import com.awesomecalc.awesomecalculator.CalculatorFragment;
import com.awesomecalc.awesomecalculator.FragmentChangerDelegate;
import com.awesomecalc.awesomecalculator.R;
import com.awesomecalc.awesomecalculator.UnitConversionFragment;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;


public class MainActivity extends ActionBarFragmentActivity
        implements FragmentChangerDelegate.OnFragmentChangedListener, NavigationView.OnNavigationItemSelectedListener {

    //view variables
    @Bind(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @Bind(R.id.drawer_navigation_view) NavigationView mNavView;

    //fragments
    CalculatorFragment mCalcFragment;
    UnitConversionFragment mUnitConvFragment;

    //other
    ActionBarDrawerToggle mDrawerToggle;
    FragmentChangerDelegate mFragChangeImpl;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        ButterKnife.bind(this);

        mCalcFragment = new CalculatorFragment();
        mUnitConvFragment = new UnitConversionFragment();

        mFragChangeImpl = new FragmentChangerDelegate(this, R.id.main_fragment_container);
        mFragChangeImpl.setOnFragmentChangedListener(this);

        mNavView.setNavigationItemSelectedListener(this);

        //dont display initial fragment when activity is being recreated
        if(savedInstanceState == null)
            mFragChangeImpl.displayFragment(mCalcFragment);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem)
    {
        menuItem.setChecked(true);
        mDrawerLayout.closeDrawer(GravityCompat.START);

        //wait for drawer to be closed
        int DRAWER_CLOSE_TIME = 400;
        Observable.just(true)
                  .delay(DRAWER_CLOSE_TIME, TimeUnit.MILLISECONDS)
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe((Object) ->
                  {
                      switch(menuItem.getItemId()) {
                          case R.id.menu_item_calculator:
                              mFragChangeImpl.displayFragment(mCalcFragment);
                              break;

                          case R.id.menu_item_unit_conversion:
                              mFragChangeImpl.displayFragment(mUnitConvFragment);
                              break;
                      }
                  });

        return true;
    }

    @Override
    protected void onActionBarChanged(Toolbar actionBar)
    {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,  mDrawerLayout, actionBar,
                R.string.app_name , R.string.app_name)  {
            public void onDrawerClosed(View view)
            {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        Fragment currentFragment = getCurrentFragment();

        if(currentFragment != null && currentFragment instanceof ActionBarFragment)
            setSupportActionBar(((ActionBarFragment) currentFragment).getSupportActionBar());
    }

    @Override
    public void onBackPressed()
    {
        if(!mFragChangeImpl.onBackPressed())
            super.onBackPressed();
    }

    @Override
    public void onFragmentChanged(Fragment newFragment)
    {
        //bind activity to the fragment's actionbar after fragment has finished inflating
        if(newFragment instanceof ActionBarFragment) {
            ((ActionBarFragment) newFragment).setOnInflatedListener(()
                    -> setSupportActionBar(((ActionBarFragment) newFragment).getSupportActionBar()));
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState)
    {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.syncState();
    }

    @Override
    protected Fragment getCurrentFragment()
    {
        return (mFragChangeImpl != null) ? mFragChangeImpl.getCurrentFragment() : null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings)
        {
            // TODO: Open Settings activity here
            return true;
        }

        else if(id == R.id.action_help) {
            // TODO: Open Help activity here
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
