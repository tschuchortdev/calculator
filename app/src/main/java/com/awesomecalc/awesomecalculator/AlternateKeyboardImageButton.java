package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Thilo on 1/4/2016.
 */
public class AlternateKeyboardImageButton extends ImageButton implements KeyboardButton.AlternateButton, View.OnClickListener {
    protected String mKeyCode;
    protected KeyboardButton mParentButton;

    public AlternateKeyboardImageButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context.obtainStyledAttributes(attrs, R.styleable.AlternateKeyboardImageButton, 0, 0));
    }

    public AlternateKeyboardImageButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context.obtainStyledAttributes(attrs, R.styleable.AlternateKeyboardImageButton, 0, 0));
    }

    private void init(TypedArray typedAttrs)
    {
        if(typedAttrs != null) {
            mKeyCode = typedAttrs.getString(R.styleable.AlternateKeyboardButton_keyCode);

            if(mKeyCode == null) {
                throw new IllegalStateException("AlternateKeyboardImageButton must be created with keyCode attribute");
            }
        }
        else {
            throw new NullPointerException("AlternateKeyboardImageButton must have attributes");
        }

        setAdjustViewBounds(true);
        setOnClickListener(this);
    }

    @Override
    public void setParentButton(KeyboardButton parentButton)
    {
        mParentButton = parentButton;
    }

    public void setKeyCode(@NonNull String newKeyCode)
    {
        mKeyCode = newKeyCode;
    }

    public String getKeyCode()
    {
        return mKeyCode;
    }

    @Override
    public void onClick(View v)
    {
        mParentButton.getParentMathKeyboard().keyCodeClicked(mKeyCode);
    }
}
