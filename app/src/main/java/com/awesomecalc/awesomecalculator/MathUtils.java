package com.awesomecalc.awesomecalculator;

import android.graphics.Point;
import android.graphics.PointF;

/**
 * Created by Thilo on 4/8/2016.
 */
public class MathUtils {
    public static double distance(Point p, Point q)
    {
        return distance(p.x, p.y, q.x, q.y);
    }

    public static double distance(PointF p, PointF q)
    {
        return distance(p.x, p.y, q.x, q.y);
    }

    public static double distance(float x1, float y1, float x2, float y2)
    {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    public static short max(short... args)
    {
        short result = 0;

        for(short f : args)
            Math.max(result, f);

        return result;
    }

    public static long max(long... args)
    {
        long result = 0;

        for(long f : args)
            result = Math.max(result, f);

        return result;
    }

    public static int max(int... args)
    {
        int result = 0;

        for(int f : args)
            result = Math.max(result, f);

        return result;
    }

    public static float max(float... args)
    {
        float result = 0;

        for(float f : args)
            result = Math.max(result, f);

        return result;
    }

    public static double max(double... args)
    {
        double result = 0;

        for(double f : args)
            result = Math.max(result, f);

        return result;
    }

    public static boolean floatEqual(double f1, double f2, double delta)
    {
        return Math.abs(f1-f2) < delta;
    }
}
