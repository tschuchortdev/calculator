package com.awesomecalc.awesomecalculator;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/**
 * Created by Thilo on 11/17/2015.
 *
 * can only be used with DrawerActivity
 */
public abstract class ActionBarFragment extends Fragment {
    protected FragmentInflatedListener mInflatedListener;
    protected Integer mActionBarColor;
    protected Integer mStatusBarColor;

    public interface FragmentInflatedListener {
        void onFragmentInflated();
    }

    //restore fragment state here
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            setStatusBarColor(savedInstanceState.getInt("statusBarColor", UIUtils.getColorFromTheme(getActivity(), android.R.attr.statusBarColor)));
            setActionBarColor(savedInstanceState.getInt("actionBarColor", UIUtils.getColorFromTheme(getActivity(), R.attr.colorPrimary)));
        }
        else {
            if(mStatusBarColor != null)
                UIUtils.setStatusBarColor(getActivity(), mStatusBarColor);
            else
                UIUtils.setStatusBarColor(getActivity(), UIUtils.getColorFromTheme(getActivity(), android.R.attr.statusBarColor));

            if(mActionBarColor != null)
                getSupportActionBar().setBackgroundColor(mActionBarColor);

            getSupportActionBar().setTitle(getTitle());
        }

        if(getActivity() instanceof AppCompatActivity)
            ((AppCompatActivity) getActivity()).setSupportActionBar(getSupportActionBar());
        else
            throw new IllegalArgumentException("ActionBarFragment can only be used with AppCompatActivity");

        if(mInflatedListener != null)
            mInflatedListener.onFragmentInflated();
    }

    //save fragment state here
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if(mActionBarColor != null)
            outState.putInt("actionBarColor", mActionBarColor);

        if(mStatusBarColor != null)
            outState.putInt("statusBarColor", mStatusBarColor);
    }

    public void setOnInflatedListener(FragmentInflatedListener listener)
    {
        mInflatedListener = listener;
    }

    public void removeOnInflatedListener()
    {
        mInflatedListener = null;
    }

    public void setActionBarColor(int color)
    {
        mActionBarColor = color;
        getSupportActionBar().setBackgroundColor(color);
    }

    public void setStatusBarColor(int color)
    {
        mStatusBarColor = color;
        UIUtils.setStatusBarColor(getActivity(), color);
    }

    public int getFragmentSpecificActionBarColor()
    {
        return mActionBarColor;
    }

    public int getFragmentSpecificStatusBarColor()
    {
        return mStatusBarColor;
    }

    public abstract Toolbar getSupportActionBar();

    /**
     * gets the title that will be displayed in the actionbar
     * @return actionbar title
     */
    public abstract String getTitle();
}
