package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 * Created by Thilo on 2/1/2016.
 *
 * lazy factory that creates Typeface singletons from font assets to prevent memory leaks and I/O slow downs
 */
public class FontFactory {
    private static final HashMap<String, Typeface> sFontSingletonsMap = new HashMap<>();

    public enum Style {
        BLACK,
        BLACK_ITALIC,
        BOLD,
        BOLD_ITALIC,
        ITALIC,
        LIGHT,
        LIGHT_ITALIC,
        MEDIUM,
        MEDIUM_ITALIC,
        REGULAR,
        THIN,
        THIN_ITALIC,
        CONDENSED_BOLD,
        CONDENSED_BOLD_ITALIC,
        CONDENSED_ITALIC,
        CONDENSED_LIGHT,
        CONDENSED_LIGHT_ITALIC,
        CONDENSED_REGULAR
    }

    public static synchronized Typeface fromAsset(Context ctx, String assetPath)
    {
        Typeface fontSingleton = sFontSingletonsMap.get(assetPath);

        if(fontSingleton == null) {
            fontSingleton = Typeface.createFromAsset(ctx.getAssets(), assetPath);
            sFontSingletonsMap.put(assetPath, fontSingleton);
        }

        return fontSingleton;
    }

    public static Typeface getRoboto(Context ctx)
    {
        return getRoboto(ctx, Style.REGULAR);
    }

    public static Typeface getRoboto(Context ctx, Style style)
    {
        switch(style)
        {
            case BLACK:
                return fromAsset(ctx, "fonts/Roboto-Black.ttf");
            case BLACK_ITALIC:
                return fromAsset(ctx, "fonts/Roboto-BlackItalic.ttf");
            case BOLD:
                return fromAsset(ctx, "fonts/Roboto-Bold.ttf");
            case BOLD_ITALIC:
                return fromAsset(ctx, "fonts/Roboto-BoldItalic.ttf");
            case ITALIC:
                return fromAsset(ctx, "fonts/Roboto-Italic.ttf");
            case LIGHT:
                return fromAsset(ctx, "fonts/Roboto-Light.ttf");
            case LIGHT_ITALIC:
                return fromAsset(ctx, "fonts/Roboto-LightItalic.ttf");
            case MEDIUM:
                return fromAsset(ctx, "fonts/Roboto-Medium.ttf");
            case MEDIUM_ITALIC:
                return fromAsset(ctx, "fonts/Roboto-MediumItalic.ttf");
            case REGULAR:
                return fromAsset(ctx, "fonts/Roboto-Regular.ttf");
            case THIN:
                return fromAsset(ctx, "fonts/Roboto-Thin.ttf");
            case THIN_ITALIC:
                return fromAsset(ctx, "fonts/Roboto-ThinItalic.ttf");
            case CONDENSED_BOLD:
                return fromAsset(ctx, "fonts/RobotoCondensed-Bold.ttf");
            case CONDENSED_BOLD_ITALIC:
                return fromAsset(ctx, "fonts/RobotoCondensed-BoldItalic.ttf");
            case CONDENSED_ITALIC:
                return fromAsset(ctx, "fonts/RobotoCondensed-Italic.ttf");
            case CONDENSED_LIGHT:
                return fromAsset(ctx, "fonts/RobotoCondensed-Light.ttf");
            case CONDENSED_LIGHT_ITALIC:
                return fromAsset(ctx, "fonts/RobotoCondensed-LightItalic.ttf");
            case CONDENSED_REGULAR:
                return fromAsset(ctx, "fonts/RobotoCondensed-Regular.ttf");
            default:
                throw new IllegalArgumentException("invalid enum constant");
        }
    }
}
