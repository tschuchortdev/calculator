package com.awesomecalc.awesomecalculator;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Region;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;

/**
 * Created by Thilo on 4/7/2016.
 */
public class RippleFillView extends FrameLayout
{
    protected int mColor;
    protected PointF mOrigin;
    protected int mFillTime = 1000;
    protected int mFadeOutTime = 200;
    protected ValueAnimator mRadiusAnimator;
    protected ValueAnimator mFadeOutAnimator;
    protected boolean mIsAnimating;

    private Path mPath;
    private int mCurrentRadius;
    private float mCurrentAlpha;
    private Paint mPaint;

    public interface EndAction extends Runnable {}

    public RippleFillView(@NonNull Context ctx)
    {
        this(ctx, null);
    }

    public RippleFillView(@NonNull Context ctx, @Nullable AttributeSet attrs)
    {
        this(ctx, attrs, 0);
    }

    public RippleFillView(@NonNull Context ctx, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(ctx, attrs, defStyleAttr);

        mPaint = new Paint();
        mPath = new Path();

        setClipChildren(true);

        if(attrs != null) {
            TypedArray typedAttrs = ctx.obtainStyledAttributes(attrs, R.styleable.RippleFillView, 0, 0);

            mOrigin = new PointF(
                    typedAttrs.getFloat(R.styleable.RippleFillView_originX, 0),
                    typedAttrs.getFloat(R.styleable.RippleFillView_originY, 0));

            mColor = typedAttrs.getColor(R.styleable.RippleFillView_android_color, 0);
            mFillTime = typedAttrs.getInt(R.styleable.RippleFillView_fillTime, mFillTime);
            mFadeOutTime = typedAttrs.getInt(R.styleable.RippleFillView_fadeOutTime, mFadeOutTime);

            typedAttrs.recycle();
        }

        if(mOrigin == null)
            mOrigin = new PointF();
    }

    public void setColor(int color)
    {
        mColor = color;
    }

    public int getColor()
    {
        return mColor;
    }

    public void setFillTime(int fillTime)
    {
        mFillTime = fillTime;
    }

    public int getFillTime()
    {
        return mFillTime;
    }

    public void setOrigin(float x, float y)
    {
        setOrigin(new PointF(x, y));
    }

    public void setOrigin(@NonNull PointF origin)
    {
        mOrigin = origin;
    }

    public PointF getOrigin()
    {
        return mOrigin;
    }

    public boolean isAnimating()
    {
        return mIsAnimating;
    }

    public void ripple()
    {
        rippleWithEndAction(null);
    }

    protected void startFadeOutAnimation()
    {
        mFadeOutAnimator = ValueAnimator.ofFloat(1, 0);
        mFadeOutAnimator.setDuration(mFadeOutTime);
        mFadeOutAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        mFadeOutAnimator.addUpdateListener((ValueAnimator animation) ->
        {
            mCurrentAlpha = (Float) animation.getAnimatedValue();
            invalidate();
        });

        mFadeOutAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation)
            {}

            @Override
            public void onAnimationEnd(Animator animation)
            {
                mCurrentRadius = 0;
            }

            @Override
            public void onAnimationCancel(Animator animation)
            {
                mCurrentAlpha = 0;
                mIsAnimating = false;
                invalidate();
            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {}
        });

        mFadeOutAnimator.start();
    }

    public void rippleWithEndAction(@Nullable EndAction endAction)
    {
        if (mIsAnimating)
            mRadiusAnimator.cancel();

        //set target radius to the distance between the ripple origin and furthest away corner of the view
        int targetRadius = MathUtils.max(
                (int) MathUtils.distance(mOrigin.x, mOrigin.y, 0, 0),  //top left
                (int) MathUtils.distance(mOrigin.x, mOrigin.y, getWidth(), 0), //top right
                (int) MathUtils.distance(mOrigin.x, mOrigin.y, 0, getHeight()), //bottom left
                (int) MathUtils.distance(mOrigin.x, mOrigin.y, getWidth(), getHeight())); //bottom right

        mRadiusAnimator = ValueAnimator.ofInt(0, targetRadius);
        mRadiusAnimator.setDuration(mFillTime);
        mRadiusAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        mRadiusAnimator.addUpdateListener((ValueAnimator animation) ->
                {
                    mCurrentRadius = (Integer) animation.getAnimatedValue();
                    invalidate();
                });

        mRadiusAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator)
                {
                    mIsAnimating = true;
                    mCurrentAlpha = 1;
                }

                @Override
                public void onAnimationEnd(Animator animator)
                {
                    if(endAction != null)
                        endAction.run();

                    startFadeOutAnimation();
                }

                @Override
                public void onAnimationCancel(Animator animator)
                {
                    mFadeOutAnimator.cancel();
                    mCurrentRadius = 0;
                    mIsAnimating = false;
                    invalidate();
                }

                @Override
                public void onAnimationRepeat(Animator animator)
                {}
            });

        mRadiusAnimator.start();
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        //clip everything behind the ripple when the ripple has no alpha
        if(MathUtils.floatEqual(mCurrentAlpha, 1, 0.01f)) {
            canvas.save(Canvas.CLIP_SAVE_FLAG);
            mPath.reset();
            mPath.addCircle(mOrigin.x, mOrigin.y, mCurrentRadius, Path.Direction.CW);
            canvas.clipPath(mPath, Region.Op.XOR);

            super.dispatchDraw(canvas);

            canvas.restore();
        }
        else
            super.dispatchDraw(canvas);

        if (isInEditMode())
            return;

        mPaint.setColor(mColor);
        mPaint.setAlpha((int) (0xFF * mCurrentAlpha));
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        canvas.save(Canvas.CLIP_SAVE_FLAG);
        canvas.clipRect(0, 0, getRight(), getBottom());
        canvas.drawCircle(mOrigin.x, mOrigin.y, mCurrentRadius, mPaint);
        canvas.restore();
    }
}
