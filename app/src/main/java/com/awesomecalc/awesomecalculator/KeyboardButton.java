package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A button to be used with a MathKeyboard
 *
 * Created by Thilo on 26.01.2015.
 */
public class KeyboardButton extends ViewGroup implements View.OnLongClickListener, View.OnClickListener {
    private LinearLayout mAltButtonPopup;
    private MathKeyboard mParentMathKeyboard;
    private View mPrimaryView;
    private TypedArray mTypedAttributes;
    private final int SECONDARY_BUTTONS_MARGIN_TO_KEYBOARD_BOUNDS = 5;
    private final int SECONDARY_BUTTONS_OFFSET_OVER_PRIMARY = 10;
    private final double SECONDARY_BUTTON_SIZE = 1/5f;
    private final int DEFAULT_TEXT_SIZE_DP = 22;
    private String mKeyCode;
    private int mRowCount = 1;
    private boolean mHasAltButtons = false;
    private volatile boolean mPopupTouchable = false;

    public interface AlternateButton {
        void setParentButton(KeyboardButton parentButton);
    }

    public KeyboardButton(Context ctx)
    {
        super(ctx);
        init();
    }

    public KeyboardButton(Context ctx, AttributeSet attrs)
    {
        super(ctx, attrs);
        mTypedAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.KeyboardButton, 0, 0);
        init();
    }

    public KeyboardButton(Context ctx, AttributeSet attrs, int defStyle)
    {
        super(ctx, attrs, defStyle);
        mTypedAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.KeyboardButton, 0, 0);
        init();
    }

    public MathKeyboard getParentMathKeyboard()
    {
        if(mParentMathKeyboard != null)
            return mParentMathKeyboard;
        else
            throw new NullPointerException("parent MathKeyboard is null (possibly called before onAttachedToWindow()?)");
    }

    @Override
    public void setPadding(int left, int top, int right, int bottom)
    {
        if(mPrimaryView != null)
            mPrimaryView.setPadding(left, top, right, bottom);
        else
            super.setPadding(left, top, right, bottom);
    }

    private void init()
    {
        String text = null;
        Drawable image = null;
        int textSize = 0;
        ColorStateList textColor = null;
        Drawable backgroundDrawable = null;

        setClickable(true);
        setLongClickable(true);
        setOnClickListener(this);
        setOnLongClickListener(this);

        if(mTypedAttributes != null) {
            mKeyCode = mTypedAttributes.getString(R.styleable.KeyboardButton_keyCode);
            mRowCount = mTypedAttributes.getInt(R.styleable.KeyboardButton_rows, 1);
            text = mTypedAttributes.getString(R.styleable.KeyboardButton_android_text);
            textSize = mTypedAttributes.getDimensionPixelSize(R.styleable.KeyboardButton_android_textSize, 0);
            image = mTypedAttributes.getDrawable(R.styleable.KeyboardButton_image);
            textColor = mTypedAttributes.getColorStateList(R.styleable.KeyboardButton_android_textColor);
            backgroundDrawable = mTypedAttributes.getDrawable(R.styleable.KeyboardButton_android_background);
        }

        if(text != null && image != null)
            throw new IllegalArgumentException("KeyboardButton can not have both text and image");

        if(text != null){
            mPrimaryView = new TextView(getContext());
            ((TextView) mPrimaryView).setText(text);
            ((TextView) mPrimaryView).setGravity(Gravity.CENTER);

            if(textSize != 0)
                ((TextView) mPrimaryView).setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            else
                ((TextView) mPrimaryView).setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE_DP);

            if(textColor != null)
                ((TextView) mPrimaryView).setTextColor(textColor);
        }
        else if(image != null) {
            mPrimaryView = new ImageView(getContext());
            ((ImageView) mPrimaryView).setImageDrawable(image);
        }
        else
            throw new IllegalStateException("KeyboardButton needs either text or image");

        if(backgroundDrawable != null)
            setBackground(backgroundDrawable);

        /* we want to intercept all padding changes in this view and instead apply them to the childView because the ripple-effect's size is dependent on the child's size
           otherwise the ripple size would be different for KeyboardButtons who have a padded ImageView as child
           padding that is specified in XML is set in the super constructor post-LOLLIPOP and via the setPadding() method in pre-LOLLIPOP, so we have to handle both cases
           differently depending on the OS version */
        mPrimaryView.setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());

        addView(mPrimaryView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        if(mKeyCode == null)
            throw new IllegalStateException("KeyboardButton must have a key code");

        if(mRowCount < 1)
            throw new AssertionError("KeyboardButton rowCount attribute can not be smaller than 1");
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();

        if(mParentMathKeyboard == null) {
            mParentMathKeyboard = (MathKeyboard) UIUtils.findGrandparent(this, (View testable) -> testable instanceof MathKeyboard);

            if(mParentMathKeyboard == null)
                throw new IllegalStateException("KeyboardButton must have a parent/grandparent of type MathKeyboard");
        }

        if(mAltButtonPopup != null) {
            //set elevation to higher than FAB because in Lollipop the elevation takes precedence over index in FrameLayout z-ordering
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                mAltButtonPopup.setElevation(mParentMathKeyboard.getFab().getElevation()+1);

            //if not yet added to parent
            if(mAltButtonPopup.getParent() == null)
                mParentMathKeyboard.getParentFrameLayout().addView(mAltButtonPopup, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        }
    }

    void addChildToAltButtonPopup(@NonNull View child)
    {
        int buttonSize = (int) (UIUtils.getScreenWidth(getContext()) * SECONDARY_BUTTON_SIZE);

        //create altButtonPopup if not already created
        if(mAltButtonPopup == null) {
            mAltButtonPopup = new LinearLayout(getContext());
            mAltButtonPopup.setOrientation(LinearLayout.HORIZONTAL);
            mAltButtonPopup.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.keyboard_popup_background));
            mAltButtonPopup.setVisibility(GONE);

            if(mParentMathKeyboard != null)
                mParentMathKeyboard.getParentFrameLayout().addView(mAltButtonPopup, new LayoutParams(buttonSize, buttonSize));
        }

        mAltButtonPopup.addView(child, new LinearLayout.LayoutParams(buttonSize, buttonSize));
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params)
    {
        //first child is the TextView/ImageView for this KeyboardButton
        if(getChildCount() == 0) {
            super.addView(child, index, params);
        }
        //following childs are alternate buttons
        else {
            if(!(child instanceof AlternateButton))
                throw new RuntimeException("KeyboardButton can not have children of a type other than AlternateButton");

            ((AlternateButton) child).setParentButton(this);

            //if more than one AlternateButton is added, they will all be put into a popup Layout
            if(mHasAltButtons) {
                if(getChildCount() == 2) {
                    //remove the first alt button and add it to the popup layout
                    View  firstAltButton = getChildAt(1);
                    removeView(firstAltButton);
                    firstAltButton.setVisibility(VISIBLE);
                    addChildToAltButtonPopup(firstAltButton);
                }

                addChildToAltButtonPopup(child);
            }
            //second child is the first AlternateButton that will appear instead of the TextView when long-clicked
            else {
                child.setVisibility(GONE);
                super.addView(child, index, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
                mHasAltButtons = true;
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        return true;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev)
    {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if(mAltButtonPopup != null && mPopupTouchable) {
                    //hit test and hover
                    for (int i = 0; i < mAltButtonPopup.getChildCount(); i++) {
                        Rect secondaryButtonArea = UIUtils.getViewBounds(mAltButtonPopup.getChildAt(i));

                        if (secondaryButtonArea.contains((int) ev.getRawX(), (int) ev.getRawY()))
                            mAltButtonPopup.getChildAt(i).setPressed(true);
                        else
                            mAltButtonPopup.getChildAt(i).setPressed(false);
                    }

                    return true;
                }
                else if(mPrimaryView.getVisibility() != VISIBLE)
                    return true;

            case MotionEvent.ACTION_UP:
                if(mAltButtonPopup != null && mPopupTouchable) {
                    //hit test and click
                    for (int i = 0; i < mAltButtonPopup.getChildCount(); i++) {
                        Rect secondaryButtonArea = UIUtils.getViewBounds(mAltButtonPopup.getChildAt(i));

                        if (secondaryButtonArea.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                            mAltButtonPopup.getChildAt(i).performClick();
                            mAltButtonPopup.getChildAt(i).setPressed(false);
                        }
                    }

                    //hide alt buttons
                    hideAltButtonPopup();
                }
                else if(mPrimaryView.getVisibility() != VISIBLE) {
                    Rect secondaryButtonArea = UIUtils.getViewBounds(getChildAt(1));

                    //hit test and click
                    if (secondaryButtonArea.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                        getChildAt(1).performClick();
                    }

                    //hide second button
                    getChildAt(0).setVisibility(VISIBLE);
                    getChildAt(1).setVisibility(GONE);
                }
                break;
        }

        return super.onTouchEvent(ev);
    }

    private void showAltButtonPopup()
    {
        setPressed(false);
        getParentMathKeyboard().setBlurred(true);

        mAltButtonPopup.setVisibility(VISIBLE);
        //do this onPreDraw because getHeight() has to be called after measure()
        mAltButtonPopup.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw()
            {
                mAltButtonPopup.setY(calculateAltButtonY() + mAltButtonPopup.getHeight() / 2);
                mAltButtonPopup.setX(calculateAltButtonX());
                mAltButtonPopup.setScaleX(0.0f);
                mAltButtonPopup.setScaleY(0.0f);

                mAltButtonPopup.animate()
                               .scaleX(1.0f)
                               .scaleY(1.0f)
                               .y(calculateAltButtonY())
                               .setDuration(150)
                               .setInterpolator(new AccelerateDecelerateInterpolator())
                               .withEndAction(() -> mPopupTouchable = true);

                mAltButtonPopup.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });


    }

    private void hideAltButtonPopup()
    {
        mAltButtonPopup.setPressed(false);

        mAltButtonPopup.animate()
                       .scaleX(0.0f)
                       .scaleY(0.0f)
                       .y(mAltButtonPopup.getY() + mAltButtonPopup.getHeight() / 2)
                       .setDuration(150)
                       .setInterpolator(new AccelerateDecelerateInterpolator())
                       .withEndAction(() -> {
                           mAltButtonPopup.setVisibility(GONE);
                           getParentMathKeyboard().setBlurred(false);
                           mPopupTouchable = false;
                       });
    }

    @Override
    public void onClick(View clickedView)
    {
        getParentMathKeyboard().keyCodeClicked(mKeyCode);
    }

    @Override
    public boolean onLongClick(View clickedView)
    {
        //stop parents (such as the ViewPager) from intercepting events when the altButtonPopup is shown
        getParent().requestDisallowInterceptTouchEvent(true);

        if(mAltButtonPopup != null) {
           showAltButtonPopup();
        }
        else if(getChildCount() == 2) {
            mPrimaryView.setVisibility(GONE);
            getChildAt(1).setVisibility(VISIBLE);
        }

        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec, 0, child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec, 0, child.getLayoutParams().height));
        }

        setMeasuredDimension(
                resolveSize(mPrimaryView.getMeasuredWidth(), widthMeasureSpec),
                resolveSize(mPrimaryView.getMeasuredHeight(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);

            //purposely ignore padding because this View shouldn't have padding (instead the child should be padded)
            child.layout(0, 0, child.getMeasuredWidth(), child.getMeasuredHeight());
        }
    }

    private float calculateAltButtonX()
    {
        //cache values for performance
        int primaryButtonWidth = getWidth();
        int secondaryContainerWidth = mAltButtonPopup.getWidth();

        if(secondaryContainerWidth == primaryButtonWidth)
            return getX();
        else if(secondaryContainerWidth < primaryButtonWidth) {
            int offset = (primaryButtonWidth - secondaryContainerWidth) / 2;
            return getX() + offset;
        }
        else if(secondaryContainerWidth > primaryButtonWidth) {
            int offset = (secondaryContainerWidth - primaryButtonWidth) / 2;
            float distanceLeft = getX();
            float distanceRight = UIUtils.getScreenWidth(getContext()) - (getX() + getWidth());

            if(distanceLeft < distanceRight) {
                if(distanceLeft - offset < SECONDARY_BUTTONS_MARGIN_TO_KEYBOARD_BOUNDS)
                    return SECONDARY_BUTTONS_MARGIN_TO_KEYBOARD_BOUNDS;
                else
                    return getX() - offset;
            }
            else {
                if(distanceRight - offset < SECONDARY_BUTTONS_MARGIN_TO_KEYBOARD_BOUNDS)
                    return UIUtils.getScreenWidth(getContext()) - SECONDARY_BUTTONS_MARGIN_TO_KEYBOARD_BOUNDS - mAltButtonPopup.getWidth();
                else
                    return getX() + getWidth() + offset - mAltButtonPopup.getWidth();
            }
        }
        else
            return 0.0f;
    }

    private float calculateAltButtonY()
    {
        return UIUtils.getYRelativeTo(this, getParentMathKeyboard().getParentFrameLayout()) - mAltButtonPopup.getHeight() - SECONDARY_BUTTONS_OFFSET_OVER_PRIMARY;
    }
}


