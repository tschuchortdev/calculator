package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * calculator keyboard to be used with KeyboardButton
 *
 * Created by Thilo on 11/19/2015.
 */
public class MathKeyboard extends LinearLayout {
    protected final TypedArray mAttributes;
    @Bind(R.id.blur_layout) protected BlurLayout mBlurLayout;
    @Bind(R.id.keyboard_pager) protected XmlViewPager mKeyboardPager;
    @Bind(R.id.page_indicator_layout) protected LinearLayout mPageIndicatorLayout;
    protected FloatingActionButton mSolveFab;
    protected FrameLayout mParentFrameLayout;
    protected MathKeyboardListener mListener;

    public MathKeyboard(Context ctx)
    {
        this(ctx, null);
    }

    public MathKeyboard(Context ctx, AttributeSet attrs)
    {
        this(ctx, attrs, 0);
    }

    public MathKeyboard(Context ctx, AttributeSet attrs, int defStyle)
    {
        super(ctx, attrs, defStyle);
        LayoutInflater.from(getContext()).inflate(R.layout.view_keyboard, this);
        ButterKnife.bind(this);

        mAttributes = ctx.obtainStyledAttributes(attrs, R.styleable.MathKeyboard, 0, 0);
        setOrientation(VERTICAL);

        mSolveFab = new FloatingActionButton(getContext());
        mSolveFab.setOnClickListener((View v) -> { if(mListener != null) mListener.onSolvePressed(); });
        ColorStateList solveFabColor = mAttributes.getColorStateList(R.styleable.MathKeyboard_solveFabColor);
        mSolveFab.setClickable(true);
        mSolveFab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_solve));

        if(solveFabColor != null)
            mSolveFab.setBackgroundTintList(solveFabColor);

        initPageIndicator();
    }
    public void keyCodeClicked(String keyCode)
    {
        if(keyCode != null && mListener != null) {
            switch (keyCode) {
                case "solve":
                    mListener.onSolvePressed();
                    break;

                case "backspace":
                    mListener.onBackspacePressed();
                    break;

                case "clear":
                    mListener.onClearPressed();
                    break;

                case "moveCursorLeft":
                    mListener.onMoveCursorLeftPressed();
                    break;

                case "moveCursorRight":
                    mListener.onMoveCursorRightPressed();
                    break;

                default:
                    mListener.onKeyPressed(keyCode);
            }
        }
    }

    public void setParentFrameLayout(FrameLayout layout)
    {
        mParentFrameLayout = layout;
    }

    public FrameLayout getParentFrameLayout()
    {
        return mParentFrameLayout;
    }

    private void addPageIndicatorIcon()
    {
        ImageView indicatorIconView = new ImageView(getContext());
        indicatorIconView.setImageResource(R.drawable.ic_page_indicator);
        indicatorIconView.setAdjustViewBounds(true);

        mPageIndicatorLayout.addView(indicatorIconView, new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));

        UIUtils.setMargins(indicatorIconView, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics()));
    }

    private void initPageIndicator()
    {
        for (int i = 0; i < mKeyboardPager.getPageCount(); i++)
            addPageIndicatorIcon();

        mKeyboardPager.addOnModelChangeListener(new XmlViewPager.OnModelChangeListener() {
            @Override
            public void itemAdded(View page)
            {
                addPageIndicatorIcon();
            }

            @Override
            public void itemRemoved(View page)
            {
                removeViewAt(mPageIndicatorLayout.getChildCount() - 1);
                mPageIndicatorLayout.removeViewAt(mPageIndicatorLayout.getChildCount()-1);
            }
        });

        mKeyboardPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position)
            {
                for(int i = 0; i < mPageIndicatorLayout.getChildCount(); i++) {
                    ImageView indicatorView = (ImageView) mPageIndicatorLayout.getChildAt(i);
                    indicatorView.setImageResource(R.drawable.ic_page_indicator);
                }

                if(mPageIndicatorLayout.getChildCount() > 0)
                    ((ImageView) mPageIndicatorLayout.getChildAt(position)).setImageResource(R.drawable.ic_page_indicator_selected);
            }
        });
    }

    public void setBlurred(boolean blurred)
    {
        mBlurLayout.setBlurred(blurred);
    }

    public boolean isBlurred()
    {
        return mBlurLayout.isBlurred();
    }

    public void setPageSmoothely(int pageIndex)
    {
        mKeyboardPager.setCurrentItem(pageIndex, true); //true = transition smoothly to that page
    }

    public void setPage(int pageIndex)
    {
        mKeyboardPager.setCurrentItem(pageIndex);
    }

    public int getPage()
    {
        return mKeyboardPager.getCurrentItem();
    }

    void setListener(MathKeyboardListener listener)
    {
        mListener = listener;
    }

    MathKeyboardListener getListener()
    {
        return mListener;
    }

    void removeListener()
    {
        mListener = null;
    }

    FloatingActionButton getFab()
    {
        return mSolveFab;
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();

        if(mAttributes != null) {
            final int parentFrameLayoutId = mAttributes.getResourceId(R.styleable.MathKeyboard_parentFrameLayout, 0);

            if(parentFrameLayoutId != 0) {
                mParentFrameLayout = (FrameLayout) UIUtils.findGrandparent(this,
                        (View testable) -> (testable instanceof FrameLayout && testable.getId() == parentFrameLayoutId));

                if(mParentFrameLayout == null)
                    throw new IllegalArgumentException("invalid parentFrameLayout id");
            }

            float fabSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56, getResources().getDisplayMetrics()); //56dp as defined in the google design guide

            //onAttached to window also gets called when the fragment gets recreated, so the fab may already be added to the parentFrameLayout
            if(mSolveFab.getParent() == null)
                mParentFrameLayout.addView(mSolveFab, new ViewGroup.LayoutParams((int)fabSize, (int)fabSize));
        }
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state)
    {
        super.onRestoreInstanceState(state);
    }

    @Override
    protected Parcelable onSaveInstanceState()
    {
        return super.onSaveInstanceState();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);

        mSolveFab.setY(UIUtils.getYRelativeTo(this, mParentFrameLayout) - mSolveFab.getMeasuredHeight() / 2f);
        mSolveFab.setX(UIUtils.getXRelativeTo(this, mParentFrameLayout) + getWidth() * 3 / 4f - mSolveFab.getMeasuredWidth() / 2f);
    }

    public interface MathKeyboardListener {
        void onKeyPressed(String input);
        void onSolvePressed();
        void onBackspacePressed();
        void onClearPressed();
        void onMoveCursorRightPressed();
        void onMoveCursorLeftPressed();
    }

}
