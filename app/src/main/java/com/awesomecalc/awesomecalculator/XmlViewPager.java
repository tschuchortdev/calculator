package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Thilo on 11/19/2015.
 */
public class XmlViewPager extends ViewPager {
    XmlPagerAdapter mAdapter;
    ArrayList<OnModelChangeListener> mModelChangeListeners;
    int mDefaultPage;
    boolean mIsRestored;

    public XmlViewPager(Context ctx)
    {
        this(ctx, null);
    }

    public XmlViewPager(Context ctx, AttributeSet attrs)
    {
        super(ctx, attrs);

        mAdapter = new XmlPagerAdapter();
        mModelChangeListeners = new ArrayList<>();
        setAdapter(mAdapter);

        if(attrs != null) {
            TypedArray typedAttrs = getContext().obtainStyledAttributes(attrs, R.styleable.XmlViewPager, 0, 0);

            //set currently selected page non-smoothly
            mDefaultPage = typedAttrs.getInteger(R.styleable.XmlViewPager_defaultPage, 0);
            typedAttrs.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();

        //set default page after all views are added and onRestore was called (only when the view is not being restored after saving instance state)
        if(!mIsRestored)
            setCurrentItem(mDefaultPage, false);
    }

    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        super.onRestoreInstanceState(state);
        mIsRestored = (state != null);
    }

    public void addOnModelChangeListener(OnModelChangeListener listener)
    {
        if(!mModelChangeListeners.contains(listener))
            mModelChangeListeners.add(listener);
    }

    public ArrayList<OnModelChangeListener> getOnModelChangeListeners()
    {
        return mModelChangeListeners;
    }

    public void removeOnModelChangeListener(OnModelChangeListener listener)
    {
        mModelChangeListeners.remove(listener);
    }

    public int getPageCount()
    {
        return mAdapter.pages.size();
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params)
    {
        if(!mAdapter.pages.contains(child)) {
            if(index == -1)
                mAdapter.pages.add(child);
            else
                mAdapter.pages.add(index, child);

            for (OnModelChangeListener listener : mModelChangeListeners) {
                listener.itemAdded(child);
            }

            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void removeViewAt(int index)
    {
        View v = mAdapter.pages.get(index);

        for (OnModelChangeListener listener : mModelChangeListeners) {
            listener.itemRemoved(v);
        }

        mAdapter.pages.remove(index);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeAllViews()
    {
        removeViews(0, getChildCount());
    }

    @Override
    public void removeViews(int start, int count)
    {
        for(int i=0; i < count; i++) {
            View v = mAdapter.pages.get(start + i);
            mAdapter.pages.remove(start + i);

            for (OnModelChangeListener listener : mModelChangeListeners) {
                listener.itemRemoved(v);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeView(View view)
    {
        mAdapter.pages.remove(view);
        mAdapter.notifyDataSetChanged();
    }

    protected void addPage(View v)
    {
        super.addView(v, -1, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    protected void removePage(View v)
    {
        super.removeView(v);
    }

    private class XmlPagerAdapter extends PagerAdapter {
        public final ArrayList<View> pages = new ArrayList<>();

        public XmlPagerAdapter() {}

        @Override
        public int getCount()
        {
            return pages.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object)
        {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position)
        {
            View page = pages.get(position);

            if(page == null)
                throw new IllegalArgumentException("no page for that position");

            addPage(page);
            return page;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object)
        {
            removePage((View) object);
        }
    }

    public interface OnModelChangeListener {
        void itemAdded(View page);
        void itemRemoved(View page);
    }
}
